#!/bin/sh
#	Date: 6/7/20
#	Author: shin0x0b
#	Purpose: Basic steps for installing
#	an Arch Linux VM on VirtualBox
#	Depends: Arch Linux Live ISO


red=$(tput setaf 1)
white=$(tput setaf 7)
green=$(tput setaf 2)
blue=$(tput setaf 4)
yellow=$(tput setaf 3)	
the_disk=""

pingnet() {
	echo "nameserver 8.8.8.8" > /etc/resolv.conf
	dhcpcd enp0s3
	ping -c 3 8.8.8.8
	sleep 1
}

getdisk () {
    echo "What disk do you want to use?\n"
    read the_disk
}

partition() {
	clear
	printf "\033[1m ${red} Make Four Partitions \n\033[0m"
	printf "\033[1m ${yellow} /dev/nvme0n1p1 +1M ef02 for hex\n\033[0m"
	printf "\033[1m ${yellow} /dev/nvme0n1p2 +550M ef00 for hex\n\033[0m"
	printf "\033[1m ${yellow} /dev/nvme0n1p3 enter enter 8309 hex\n\033[0m"
	echo "\n"
	printf "\033[1m ${blue} o\n\033[0m"
	printf "\033[1m ${blue} b\n\033[0m"
	printf "\033[1m ${blue} [Enter]\n\033[0m"
	printf "\033[1m ${blue} 0\n\033[0m"
	printf "\033[1m ${blue} +1M\n\033[0m"
	printf "\033[1m ${blue} ef02\n\033[0m"
	printf "\033[1m ${blue} n\n\033[0m"
	printf "\033[1m ${blue} [Enter]\n\033[0m"
	printf "\033[1m ${blue} [Enter]\n\033[0m"
	printf "\033[1m ${blue} +550M\n\033[0m"
	printf "\033[1m ${blue} ef00\n\033[0m"
	printf "\033[1m ${blue} [Enter]\n\033[0m"
	printf "\033[1m ${blue} [Enter]\n\033[0m"
	printf "\033[1m ${blue} [Enter]\n\033[0m"
	printf "\033[1m ${blue} 8309\n\033[0m"
	printf "\033[1m ${blue} w\n\033[0m"
	printf "\n"
	printf "Follow instructions from:\n"
	printf "https://gist.github.com/huntrar/e42aee630bee3295b2c671d098c81268\n"
	read Enter
	fdisk /dev/nvme0n1
}

cryptsetup () {
	cryptsetup luksFormat --type luks1 --use-random -S 1 -s 512 -h sha512 -i 200 /dev/nvme0n1p3
	cryptsetup open /dev/nvme0n1p3 shinobi
	pvcreate /dev/mapper/shinobi
	vgcreate vg /dev/mapper/shonibi
	lvcreate -L 32G vg -n swap
	lvcreate -L 137G vg -n root
	lvcreate -l 100%FREE vg -n home
}

formatdrives() {
	clear
	mkfs.ext4 /dev/vg/root
	mkfs.ext4 /dev/vg/home
	mkswap /dev/vg/swap
	mkfs.fat -F32 /dev/nvme0n1p2
} 


mountparts() {
	clear
	mount /dev/vg/root /mnt
	mkdir /mnt/home
	mount /dev/vg/home /mnt/home
	swapon /dev/vg/swap
	mkdir /mnt/efi
	mount /dev/nvme0n1p2 /mnt/efi
}

confirmMount() {
	clear
	printf " \033[1m ${red} HEY!${green} You!!!${white} confirm everything is mounted correctly!\n \033[0m "
	lsblk -f
	printf "\033[1m ${green} Press Enter to Continue \n\033[0m"
	read Enter
}

fstabulation(){
	genfstab -U -p /mnt >> /mnt/etc/fstab
	printf " \033[1m ${red} HEY!${green} LOOK!\n${white} Make sure this looks correct!\n \033[0m "
	cat /mnt/etc/fstab
	sleep 7
}

mirrors(){
	clear
	printf "\033[1m ${green}Pick your mirrors!${white}\n\033[0m "
	printf "\033[1m ${red}Bring your favorites to the top! ;)${white}\n\033[0m "
	printf "\033[1m ${yellow}Hint: alt+6 = copy${white}\n\033[0m "
	printf "\033[1m ${yellow}and ctrl+U = paste.${white}\n\033[0m "
	printf "\033[1m ${green}Press Enter to Continue\n\033[0m"
	read Enter
	nano /etc/pacman.d/mirrorlist
}

pacstrap () {
	pacstrap /mnt base base-devel linux linux-firmware mkinitcpio\
	lvm2 vim dhcpcd wpa_supplicant iw dialog dhcpcd netctl
}

nextsteps() {
	#wget https://raw.githubusercontent.com/t60r/Arch-VM-Installer/master/postchroot.sh
	cp postroot.sh /mnt
	printf " \033[1m ${green}You will now be placed${white}\n\033[0m "
	printf " \033[1m ${red}into a chroot environment${white}\n\033[0m "
	printf " \033[1m ${yellow}You will be prompted for${white}\n\033[0m "
	printf " \033[1m ${yellow}a few things along the way${white}\n\033[0m "
	printf " \033[1m ${green}Press Enter to Continue\n\033[0m"
	read Enter
	chroot /mnt /bin/bash postchroot.sh
}

main() {
	pingnet
	lsblk
	partition		# does partitioning
	cryptsetup		# sets up encrypted container etc
	formatdrives	# dont fuck this up
	mountparts		# mounts your partitions
	confirmMount	# shows you exactly what you just mounted
	mirrors	
	fstabulation	# echos a few US mirrors into the mirrorlist
	pacstrap		# installs base and base-devel package groups	# generates fstab and shows the user the file
	nextsteps		# informs the user what to do next
}

main ${*}
